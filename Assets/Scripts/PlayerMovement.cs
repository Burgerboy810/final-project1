﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {
	public Text healthText;
	public float speed = 1f;
	public float max=2f;
	public AudioClip rocketExplode;
    public AudioClip Bonus;
	public Transform lowObject;
	public Transform Respawn;
    public float jumpHeight;
    

    private int healthPerSoda = 1;
	private int healthPerFruit = 2;
	private Animator animator;
	public int playerHealth = 5;
	private int secondsUntilNextLevel = 1;
  

    // Use this for initialization
    void Start () {
		animator = GetComponent<Animator>();
		healthText.text = "Health: " + playerHealth;
	}

	// Update is called once per frame
	void Update () {
        

            if (Input.GetKey (KeyCode.D))
			transform.position += new Vector3 (speed * Time.deltaTime, 0.0f, 0.0f);
		if(Input.GetKey (KeyCode.A))
			transform.position -= new Vector3 (speed * Time.deltaTime, 0.0f, 0.0f);
		if (Input.GetKey (KeyCode.W))
			transform.position += new Vector3 (0.0f, speed * Time.deltaTime, 0.0f);
		if (Input.GetKey (KeyCode.S))
			transform.position -= new Vector3 (0.0f, speed * Time.deltaTime, 0.0f);

		if (transform.position.y < lowObject.position.y)
		{
			transform.position = Respawn.position;
			animator.SetTrigger("PlayerHurt");
		}
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if(objectPlayerCollidedWith.tag == "Exit") 
		{
			Invoke("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		} 
		if(objectPlayerCollidedWith.tag == "Soda")
		{
			playerHealth += healthPerSoda;
			healthText.text = "+" + healthPerSoda + "Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(Bonus);
		}
		else if(objectPlayerCollidedWith.tag == "Fruit")
		{
			playerHealth += healthPerFruit;
			healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(Bonus);
		}
	}

	private void LoadNewLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	public void TakeDamage(int damageReceived)
	{
		playerHealth -= damageReceived;
		healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
		Debug.Log("Player Health: " + playerHealth);
	}
}